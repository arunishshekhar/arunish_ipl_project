//Function for calculating number
const matchesPerYear = (matchData) => {
    let returingObject = {}
    if (!matchData) {
        return returingObject;
    }
    for (let indivisualData of matchData) {
        if (indivisualData['season'] in returingObject) {
            returingObject[indivisualData['season']]++;
        }
        else {
            returingObject[indivisualData['season']] = 1
        }
    }
    return returingObject
}

module.exports = { matchesPerYear }