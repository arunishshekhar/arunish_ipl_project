const indexFile = require('./index')

//Data renaiming
const matchesData = indexFile.matches
const deliveriesData = indexFile.deliveries
const dataSoring = indexFile.storeToOutput

//Number of matches played per year
const matchPlayesPerYearFile = require ('./numberOfMatchesPlayedPerYear')
const matchPlayesPerYearOutput = JSON.stringify(matchPlayesPerYearFile.matchesPerYear(matchesData))
dataSoring(matchPlayesPerYearOutput,'matchesPerYear')

//Matches won per team per Year
const matchesWonPerTeamPerYearFile = require('./numberOfMatchesWonPerTeam')
const matchesWonPerTeamPerYearOutput = JSON.stringify(matchesWonPerTeamPerYearFile.matchesWonPerTeam(matchesData))
dataSoring(matchesWonPerTeamPerYearOutput,'matchesWonPerTeamPerYear')


//Extra run per team in 2016
const extraRunsPerTeamFile = require('./extraRunPerTeam')
const extraRunsPerTeamOutput = JSON.stringify(extraRunsPerTeamFile.getExtraRunsPerTeamByYear(matchesData,deliveriesData,2016))
dataSoring(extraRunsPerTeamOutput,'extraRunsPerTeam2016')

//Top 10 economical bowlers in the year 2015
const economicBowlerFile = require('./econimicalBowlersByYear')
const economicBowlerOutput = JSON.stringify(economicBowlerFile.economicalBowlerByYear(matchesData,deliveriesData,2015,10))
dataSoring(economicBowlerOutput,'economicBowler2015')
