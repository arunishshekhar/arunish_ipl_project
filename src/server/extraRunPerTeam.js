const utilisFile = require('./utlis')

const getExtraRunsPerTeamByYear = (matchFile, deliveryFile, yearRequired) => {
    let returingObject = {}
    const idOfRequiredYear = utilisFile.getIdByYear(matchFile, yearRequired);
    for (let indivisualData of deliveryFile) {
        if (indivisualData['match_id'] in idOfRequiredYear) {
            if (indivisualData['batting_team'] in returingObject) {
                returingObject[indivisualData['batting_team']] = returingObject[indivisualData['batting_team']] + indivisualData['extra_runs'];
            }
            else {
                returingObject[indivisualData['batting_team']] = indivisualData['extra_runs'];
            }
        }
    }
    return returingObject;
}

module.exports = { getExtraRunsPerTeamByYear }

