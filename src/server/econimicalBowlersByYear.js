const utilisFile = require('./utlis')

const economicalBowlerByYear = (matchesFile, deliveriesFile, dataOfYear, topHowMany) => {
    if (!matchesFile || !deliveriesFile || !dataOfYear || !topHowMany)
    {
        return [];
    }
    let bowlerObject = []
    const idOfRequiredYear = utilisFile.getIdByYear(matchesFile, dataOfYear);
    for (let indivisualData of deliveriesFile) {
        if (indivisualData['match_id'] in idOfRequiredYear) {
            if (indivisualData['bowler'] in bowlerObject) {
                bowlerObject[indivisualData['bowler']]['runs'] = bowlerObject[indivisualData['bowler']]['runs'] + indivisualData['total_runs'];
                bowlerObject[indivisualData['bowler']]['balls']++;
            }
            else {
                bowlerObject[indivisualData['bowler']] = []
                bowlerObject[indivisualData['bowler']]['runs'] = indivisualData['total_runs'];
                bowlerObject[indivisualData['bowler']]['balls'] = 1;
            }
        }
    }
    let economyCalculatedBowler = []
    for (let indivisualData in bowlerObject) {
        let tempObject = {}
        tempObject['name'] = indivisualData
        tempObject['value'] = bowlerObject[indivisualData]['runs']/bowlerObject[indivisualData]['balls']
        economyCalculatedBowler.push(tempObject)
    }
    economyCalculatedBowler.sort(function (a,b) 
    {
        var nameA = a['value'];
        var nameB = b['value']; 
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
    })
    let tempArray = economyCalculatedBowler.slice(0,topHowMany)
    let returningArray = []
    for (let indivisualData of tempArray) {
        returningArray.push(indivisualData.name)
    }
    return returningArray
}

module.exports = { economicalBowlerByYear }