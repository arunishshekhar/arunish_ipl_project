//Extracting Data from csv and converting it to json
const csvToJson = require('convert-csv-to-json');

//Matches csv to JSON
const matches = csvToJson
    .fieldDelimiter(",")
    .formatValueByType()
    .getJsonFromCsv('src/data/matches.csv');

//Deliveries csv to json
const deliveries = csvToJson
    .fieldDelimiter(",")
    .formatValueByType()
    .getJsonFromCsv('src/data/deliveries.csv');

//file Storage Fnction
const Fs = require("fs");
const storeToOutput = (data, filename) => {
    const PATH = "src/public/output/";
    Fs.writeFile(PATH + filename + ".json", data, (err) => {
        if (err) {
            throw err;
        }
        console.log("JSON data is saved.");
    });
};


//console.log(matches)
//console.log(deliveries.length)
module.exports = { matches, deliveries, storeToOutput }