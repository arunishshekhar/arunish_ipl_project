const getIdByYear = (dataOfMatches, yearForId) => {
    let returningArray = [];
    if (!dataOfMatches || !yearForId) {
        return returningArray;
    }
    for (let indivisualData of dataOfMatches) {
        if (indivisualData['season'] === yearForId) {
            returningArray.push(indivisualData['id']);
        }
    }
    return returningArray;
}
const yearList = (dataOfMatches) => {
    let returningArray = [];
    if (!dataOfMatches) {
        return returningArray;
    }
    for (let indivisualData of dataOfMatches) {
        returningArray.push(indivisualData['season'].toString())
    }
    returningArray = returningArray.filter(unique).sort()
    return returningArray;
}

const unique = (value, index, self) => {
    return self.indexOf(value) === index
}

const dataByYear = (mainData, year) =>
{
    let returingObject = []
    for (let indivisualData of mainData)
    {
        if (indivisualData['season'] === year*1)
        {
            returingObject.push(indivisualData)
        }
    }
    return returingObject
}


module.exports = { getIdByYear, yearList, dataByYear }