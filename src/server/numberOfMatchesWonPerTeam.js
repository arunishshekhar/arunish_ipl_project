const utilisFile = require('./utlis')

//Function for calculating number of matches won by team
const matchesWonPerTeam = (matchData) => {
    let returingObject = {}
    if (!matchData) {
        return returingObject;
    }
    const years = utilisFile.yearList(matchData)
    for (let year of years) {
        const tempData = utilisFile.dataByYear(matchData, year)
        let tempObj = {}
        for (let indivisualData of tempData) {
            
            if (!indivisualData['winner']) {
                indivisualData['winner'] = 'No Winner';
            }
            if (indivisualData['winner'] in tempObj) {
                tempObj[indivisualData['winner']]++;
            }
            else {
                tempObj[indivisualData['winner']] = 1
            }
        }
        returingObject[year] = tempObj;
    }
    return returingObject
}

module.exports = { matchesWonPerTeam }